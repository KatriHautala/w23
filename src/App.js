import './App.css';
import Reporter from './Reporter';

function App() {
  return (
    <div className="App">
      <h1>W23 Reporter and flying words</h1>
      <Reporter name = "Antero Mertaranta">
        Löikö mörkö sisään<br/>
        <img src="/images/morko.jpg" alt="Morko"/><br/>
      </Reporter>
      <hr/>
      <Reporter name = "Abraham Lincoln">
        Whatever you are be a good one
      </Reporter>
    </div>
  );
}

export default App;
